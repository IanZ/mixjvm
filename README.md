# MixJVM

build executable JAR with maven for clojure and java.

- build: `mvn clean package`
- run repl: 
```
$ mvn clojure:nrepl
# create remote repl config in Cursive and connect to localhost:4005
# start remote repl in Cursive
```
- run autotest in remote repl: 
```
$ (use 'midje.repl)
$ (autotest :dirs "src/main/clojure" "src/test/clojure")
# wrap all test with deftest at top level of each file so that clojure.test can recognize the results and report failure in maven
```


Reference:
http://alexott.net/en/clojure/ClojureMaven.html
