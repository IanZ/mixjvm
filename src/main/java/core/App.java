package core;

import clojure.java.api.Clojure;
import clojure.lang.IFn;

public class App {
    public static void main(String[] args) {
        IFn require = Clojure.var("clojure.core", "require");
        require.invoke(Clojure.read("mixjvm.core"));
        IFn greet = Clojure.var("mixjvm.core", "greet");
        System.out.println(greet.invoke("Ian"));
    }
}
