(ns mixjvm.core-test
  (:use midje.sweet)
  (:require [mixjvm.core :as core]
            [clojure.test :refer :all]))

(deftest basic

  (facts "Some learning tests"
         (fact "basic functions"
               (conj [1 2] 3) => [1 2 3]
               (+ 1 1) => 2
               (- 1 1) => 0)
         (fact "greet should say hi to me"
               (core/greet "Ian") => "Hello Ian"
               (core/greet "Mark") => "Hello Mark"))

  (facts "Some learning tests"
         (fact "basic functions"
               (+ 1 1) => 2
               (- 1 1) => 0)
         (fact "greet should say hi to me"
               (core/greet "Ian") => "Hello Ian"
               (core/greet "Mark") => "Hello Mark")))
